using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using net_react_tsc.Models;

namespace net_react_tsc.DAO
{
    public class TodosContext : DbContext
    {
        public DbSet<Todo> Todos { get; set; }
        public string DbPath { get; private set; }

        public TodosContext(DbContextOptions<TodosContext> options) : base(options)
        {
            var folder = Environment.SpecialFolder.LocalApplicationData;
            var path = Environment.GetFolderPath(folder);
            DbPath = $"{path}{System.IO.Path.DirectorySeparatorChar}todos.db";
            // Database.EnsureDeleted();
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite($"Data Source={DbPath}");

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Todo>().HasData(
                new Todo() {Id = 123125125, Title = "Изучить React!", Completed = false, IsDeleted = false, CreationDate = DateTime.Now, Deadline = DateTime.Now.AddHours(2)},
                new Todo() {Id = 123125126, Title = "Повторить Typescript!", Completed = true, IsDeleted = false, CreationDate = DateTime.Now, Deadline = DateTime.Now.AddHours(3)}
            );

            base.OnModelCreating(modelBuilder);
        }
    }
}