import React, { useRef } from "react";
interface TodoFormProps {
  onAdd: (title: string, deadline: string) => void;
}

export const TodoForm: React.FC<TodoFormProps> = (props) => {
  const refTitle = useRef<HTMLInputElement>(null);
  const refDeadline = useRef<HTMLInputElement>(null);

  // const keyPressHandler = (event: React.KeyboardEvent) => {
  //   if (event.key === "Enter" && refTitle.current!.value.trim() !== "") {
  //     props.onAdd(refTitle.current!.value);
  //     refTitle.current!.value = "";
  //   }
  // };

  const clickHandler = (event: React.MouseEvent) => {
    if (refTitle.current!.value.trim() !== "") {
      console.log("Deadline ",refDeadline.current!.value);      
      props.onAdd(refTitle.current!.value, refDeadline.current!.value);
      refTitle.current!.value = "";
      refDeadline.current!.value = "";
    }
  };

  return (
    <div className="todo-form">
      <div className="row mt2">
        <div className="input-field col s8 px0">
          <input
            ref={refTitle}
            type="text"
            id="title"
            placeholder="Введите название дела!"
          />
          <label htmlFor="title" className="active">
            Введите название дела!
          </label>
        </div>
        <div className="input-field col s4 px0">
          <input
            ref={refDeadline}
            type="datetime-local"
            id="deadline"
            placeholder="Введите срок исполнения!"
          />
          <label htmlFor="deadline" className="active">
            Введите срок исполнения!
          </label>
        </div>

        <a
          className="waves-effect waves-light btn blue lighten-1 modal-trigger"
          href="#modal1"
          onClick={clickHandler}
        >
          Добавить
        </a>
        <div id="modal1" className="modal">
          <div className="modal-content">
            <h4>Modal Header</h4>
            <p>A bunch of text</p>
          </div>
          <div className="modal-footer">
            <a
              href="#!"
              className="modal-close waves-effect waves-green btn-flat btn blue lighten-1"
            >
              Agree
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};
