export interface Todo {
    id: number
    title: string
    completed: boolean
    isDeleted?: boolean
    creationDate?: Date
    deadline: Date
}