import React, { useState, useEffect, useCallback } from "react";
import { TodoForm } from "../components/TodoForm";
import { TodoList } from "../components/TodoList";
import { Todo } from "../interfaces";
import { addRequest, getRequest, deleteRequest } from "../requests";

declare var confirm: (message: string) => boolean;

export const TodosPage: React.FC = () => {
  const [todos, setTodos] = useState<Todo[]>([]);

  const addHandler = (title: string, deadline: string) => {
    const newTodo: Todo = {
      id: Date.now(),
      title: title,
      completed: false,
      deadline: new Date(Date.parse(deadline))
    };

    addRequest(newTodo)
      .then((isSaved) => {
        if (isSaved) {
          setTodos((prev) => [newTodo, ...prev]);
          console.log("Todos ", todos);    
        }
      })
      .catch((message) => {
        alert(message);
      });
  };

  const toggleHandler = useCallback((id: number) => {
    setTodos(
      todos.map((todo) => {
        if (todo.id === id) {
          todo.completed = !todo.completed;
        }
        return todo;
      })
    );
  }, [todos]);

  const removeHandler = useCallback((id: number) => {
    const shouldRemove = confirm("Вы уверены что хотите удалить дело?");
    if (shouldRemove) {
      const todo = todos.find(todo => todo.id === id)
      if(!todo) {
        return
      }
      deleteRequest(todo)
      .then((isDeleted) => {
        if (isDeleted) {
          setTodos((prev) => prev.filter((todo) => todo.id !== id));
        }
      })
      .catch((message) => {
        alert(message);
      });      
    }
  }, [todos]);

  useEffect(() => {
    getRequest().then((data) => {
      if(data !== null) {                        
        const saved = data || JSON.parse("[]") as Todo[];
        setTodos(saved.map(entry => ({
          ...entry,
          deadline: new Date(entry.deadline),
          creationDate: new Date(entry.creationDate!)
        })));
      }
    });
  }, []);

  useEffect(() => {
    localStorage.setItem("todos", JSON.stringify(todos));
  }, [todos]);

  return (
    <>
      <TodoForm onAdd={addHandler} />
      <TodoList
        todos={todos}
        onToggle={toggleHandler}
        onRemove={removeHandler}
      />
    </>
  );
};
