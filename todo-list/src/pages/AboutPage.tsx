import React from 'react'
import {useHistory} from 'react-router-dom'

export const AboutPage: React.FC = () => {
    const history = useHistory()
    return <>
        <h1>Страница информации</h1>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolores exercitationem, debitis minima consequatur libero ipsa. Deserunt fugit neque suscipit quia molestiae quos accusantium laudantium qui nulla. Exercitationem, doloribus. Repellat, iure.</p>
        <button className="btn blue lighten-1" onClick={() => history.push("/")}>Обратно к списку дел</button>
    </>    
}