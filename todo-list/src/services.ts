import { Todo } from './interfaces'

export const declOfNum = (value: number, words: string[]) => {
    value = Math.abs(value) % 100;
    let num = value % 10;
    if (value > 10 && value < 20) return words[2];
    if (num > 1 && num < 5) return words[1];
    if (num == 1) return words[0];
    return words[2];
  };

export const getTimeLeft = (todo: Todo): string => {
    var diff = todo.deadline.getTime() - new Date().getTime();
    if (diff < 0) return "Истек";
    var delta = Math.abs(diff) / 1000;
    // calculate (and subtract) whole days
    var days = Math.floor(delta / 86400);
    delta -= days * 86400;
    // calculate (and subtract) whole hours
    var hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;
    // calculate (and subtract) whole minutes
    var minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;
    return (
      days.toString() +
      " " +
      declOfNum(days, ["день", "дня", "дней"]) +
      " " +
      hours.toString() +
      " " +
      declOfNum(hours, ["час", "часа", "часов"]) +
      " " +
      minutes.toString() +
      " " +
      declOfNum(minutes, ["минута", "минуты", "минут"])
    );
  };