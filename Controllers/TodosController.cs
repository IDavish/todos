using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using net_react_tsc.Models;
using net_react_tsc.DAO;
using System.Text.Json;
using System.Text;


namespace net_react_tsc.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodosController : ControllerBase
    {
        private readonly ILogger<Todo> _logger;
        private TodosContext _dbContext;

        public TodosController(ILogger<Todo> logger, TodosContext dbContext)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Todo> Get()
        {        
            return _dbContext.Todos.ToArray();
        }

        [HttpPost]
        public async Task<bool> Create(Todo todo)
        {
            try{
                todo.CreationDate = DateTime.Now;
                _dbContext.Todos.Add(todo);
                await _dbContext.SaveChangesAsync();
                return true;
            } catch {
                return false;
            }
        }

        [HttpDelete]
        public async Task<bool> Delete(Todo todo)
        {
            try{
                _dbContext.Todos.Remove(todo);
                await _dbContext.SaveChangesAsync();
                return true;
            } catch {
                return false;
            }
        }
    }
}
