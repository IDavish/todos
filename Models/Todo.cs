using System;

namespace net_react_tsc.Models {
    public class Todo {
        public long Id {get;set;}
        public string Title {get;set;}
        public bool Completed {get;set;}
        public bool IsDeleted {get;set;}
        public DateTime CreationDate {get;set;}
        public DateTime Deadline {get;set;}

        public Todo() {}

        public Todo(string title) {
            this.Title = title;
            this.IsDeleted = false;
            this.Completed = false;
        }

        public Todo(long id, string title, bool completed, DateTime creationDate, DateTime deadline) {
            this.Id = id;
            this.Title = title;
            this.Completed = completed;
            this.IsDeleted = false;
            this.CreationDate = creationDate;
            this.Deadline = deadline;
        }
    }
}